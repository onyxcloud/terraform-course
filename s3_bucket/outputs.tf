output "s3_bucket_name" {
  value = aws_s3_bucket.joshbucket.id
}

output "dynamodb_lock_name" {
  value = aws_dynamodb_table.basic-dynamodb-table.id
}
