resource "aws_s3_bucket" "joshbucket" {
  bucket = "myjosh-test-${random_string.random.id}"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "random_string" "random" {
  length  = 16
  lower   = true
  upper   = false
  special = false
}

