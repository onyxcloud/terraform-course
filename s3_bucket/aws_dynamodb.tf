resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name         = "josh-${terraform.workspace}-lock"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name        = "dynamodb-lock-${terraform.workspace}"
    Environment = "${terraform.workspace}"
  }
}