resource "aws_vpc" "master" {
  cidr_block = var.vpc_cidr

  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name                                                = "${var.cluster_name}-vpc"
    "kubernetes.io/cluster/${var.cluster_name}-cluster" = "shared"
  }
}



resource "aws_subnet" "public" {
  count = length(var.aws_availability_zones[var.region])

  vpc_id            = aws_vpc.master.id
  cidr_block        = cidrsubnet(var.vpc_cidr, 8, count.index)
  availability_zone = var.aws_availability_zones[var.region][count.index]

  tags = {
    Name                                                = "${var.cluster_name}-public-sg"
    "kubernetes.io/cluster/${var.cluster_name}-cluster" = "shared"
    "kubernetes.io/role/elb"                            = 1
  }

  map_public_ip_on_launch = true
}


# Internet Gateway
resource "aws_internet_gateway" "public" {
  vpc_id = aws_vpc.master.id

  tags = {
    "Name" = "${var.cluster_name}-igw"
  }

  depends_on = [aws_vpc.master]
}


# Route the public subnet traffic through the IGW
resource "aws_route_table" "main" {
  vpc_id = aws_vpc.master.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.public.id
  }

  tags = {
    Name = "${var.cluster_name}-Default-rt"
  }
}


# Route table and subnet associations
resource "aws_route_table_association" "internet_access" {
  count          = var.availability_zones_count
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.main.id
}


# NAT Elastic IP
resource "aws_eip" "main" {
  vpc = true

  tags = {
    Name = "${var.cluster_name}-ngw-ip"
  }
}


# NAT Gateway
resource "aws_nat_gateway" "main" {
  allocation_id = aws_eip.main.id
  subnet_id     = aws_subnet.public[0].id

  tags = {
    Name = "${var.cluster_name}-ngw"
  }
}




# Private Subnets
resource "aws_subnet" "private" {
  count = length(var.aws_availability_zones[var.region])

  vpc_id            = aws_vpc.master.id
  cidr_block        = cidrsubnet(var.vpc_cidr, 8, count.index + length(var.aws_availability_zones[var.region]) + 1)
  availability_zone = var.aws_availability_zones[var.region][count.index]

  tags = {
    Name                                                = "${var.cluster_name}-private-sg"
    "kubernetes.io/cluster/${var.cluster_name}-cluster" = "shared"
    "kubernetes.io/role/internal-elb"                   = 1
  }
}


# Add route to route table
resource "aws_route" "main" {
  route_table_id         = aws_vpc.master.default_route_table_id
  nat_gateway_id         = aws_nat_gateway.main.id
  destination_cidr_block = "0.0.0.0/0"
}



resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.master.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.main.id
  }

  tags = {
    Name = "private subnet route table"
  }
}



resource "aws_route_table_association" "private" {
  count          = length(var.aws_availability_zones[var.region])
  subnet_id      = aws_subnet.private.*.id[count.index]
  route_table_id = aws_route_table.private_route_table.id
}