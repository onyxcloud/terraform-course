variable "region" {
  description = "Default region for deploying EKS cluster"
  type        = string
  default     = "us-east-1"
}

variable "cluster_name" {
  type    = string
  default = "onyxquity-devops-workshop"
}


variable "vpc_cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
  type        = string
  default     = "10.0.0.0/16"
}


variable "subnet_cidr_bits" {
  description = "The number of subnet bits for the CIDR. For example, specifying a value 8 for this parameter will create a CIDR with a mask of /24."
  type        = number
  default     = 8
}


variable "availability_zones_count" {
  description = "The number of AZs."
  type        = number
  default     = 2
}


variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default = {
    "Project"     = "OnyxquityWorkshop"
    "Environment" = "Development"
    "Owner"       = "Joseph Ayeni"
  }
}


variable "min_node_count" {
  type    = number
  default = 2
}

variable "max_node_count" {
  type    = number
  default = 9
}

variable "machine_type" {
  type    = string
  default = "t2.small"
}


variable "aws_availability_zones" {
  type = any
  default = {
    us-east-1 = ["us-east-1a", "us-east-1b", "us-east-1c"]
  }
}