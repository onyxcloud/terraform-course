terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.region
}

terraform {
  backend "s3" {
    bucket         = "myjosh-test-bx5e5xtk5kpc5yuq"
    key            = "path/s3/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "josh-default-lock"
    encrypt        = true
  }
}