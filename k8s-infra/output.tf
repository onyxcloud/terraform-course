output "cluster_name" {
  value = aws_eks_cluster.control.name
}

output "region" {
  value = var.region
}


output "cluster_endpoint" {
  value = aws_eks_cluster.control.endpoint
}


output "cluster_ca_certificate" {
  value = aws_eks_cluster.control.certificate_authority[0].data
}